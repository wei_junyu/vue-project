export default {
    template: `
        <div id="reverse-message">
            <h2>Reversing a message</h2>
            <label for="messageToReverse"></label>
            <input id="messageToReverse" v-model="messageToReverse">
            <button v-on:click="reverseMessage">Reverse</button>
            <h3>Reversed message</h3>
            <p>{{ messageToReverse }}</p>
        </div>
    `,
    data: function() {
        return {
            messageToReverse: 'hello!'
        }
    },
    methods: {
        reverseMessage: function() {
            let reversedMessage = '';
            for (let i = this.messageToReverse.length - 1; i >= 0; --i) {
                reversedMessage += this.messageToReverse[i];
            }
            this.messageToReverse = reversedMessage;
        }
    }
}