export default {
    template: `
    <div id="vue-examples">
        <h1>VueJS Examples</h1>
        <ul>
            <li>
                <router-link to="vue-examples/reverse-message">Reverse message</router-link>
            </li>
        </ul>
        
        <router-view></router-view>
    </div>
    `
}