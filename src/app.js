import './scss/main.scss';

import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from './components/home';
import About from './components/about';
import veMain from './components/vueExamples/main';
import veReverseMessage from './components/vueExamples/reverseMessage';

const routes = [
    { path: '/home', component: Home },
    { path: '/about', component: About },
    { 
        path: '/vue-examples', 
        component: veMain,
        children: [
            {
                path: 'reverse-message',
                component: veReverseMessage
            }
        ]
    }
]

const router = new VueRouter({
    routes
})

const app = new Vue({
    el: '#app',
    router: router
})

function init() {
    app.$router.push('home');
}

init();