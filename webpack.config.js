const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const environment = process.env.NODE_ENV || "development";

module.exports = (env = {}) => {
  let config = {
    // 1. Where to start
    entry: "./src/app.js",

    // 2. Where to end
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "bundle.js"
    },

    // 3. Modules to operate on different files
    module: {
      rules: [
        {
          // transpile to es2015
          test: /\.js$/,
          use: [
            {
              loader: "babel-loader",
              options: {
                presets: ["env"]
              }
            }
          ],
          exclude: path.resolve(__dirname, 'node_modules')
        },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            use: 'css-loader'
          })
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            use: ['css-loader', 'sass-loader'],
            fallback: 'style-loader'
          })
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          use: {
            loader: 'url-loader',
            options: {
              limit: 8192
            }
          }
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          loader: "file-loader",
          options: {
            name: file => {
              return (environment === "production") ? '[hash].[ext]' : '[name].[ext]';
            }
          }
        }
      ]
    },

    // 4. Plugins to operate on entire code
    plugins: [
      new ExtractTextPlugin('[name].css'),
      new HtmlWebpackPlugin({
        title: "JY's Vue App",
        template: "index.ejs",
        inject: true
      })
    ],

    // Vuejs specific
    resolve: {
      alias: {
        vue$: "vue/dist/vue.esm.js"
      },
      extensions: ["*", ".js", ".vue", ".json"]
    },

    // webpack dev server
    devServer: {
      contentBase: path.join(__dirname, "dist")
    }
  };

  if (environment === "production") {
    config.plugins.push(new UglifyJsPlugin());
  }

  return config;
};
